/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;

/**
 *
 * @author EduardoYair
 */
public class Grayscale { 
   
    private BufferedImage bf;
    private Raster data;
    private DataBuffer datab;
    private int width, height;
    private int datav [];
    private int datanew[];
    private BufferedImage bufnew;
    
    public Grayscale(BufferedImage bfi){
        bf=bfi;
    }
     public void doGrayscale(){
  
         
        width=bf.getWidth();
        height=bf.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf.getType());
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
              int p = bf.getRGB(x,y);

              int a = p & 0xff000000;
              int r = (p>>16)&0xff;
              int g = (p>>8)&0xff;
              int b = (p)&0xff;

              //calculate average
              int avg = (r+g+b)/3;

              //replace RGB value with avg
              p = (a<<24) | (avg<<16) | (avg<<8) | avg;

              bi.setRGB(x, y, p);
            }
        }
      
        bufnew=bi;
             
        }
     public BufferedImage getImg(){
             
            return bufnew;
        }
}