/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBuffer;
import java.awt.image.Kernel;
import java.awt.image.Raster;

/**
 *
 * @author EduardoYair
 */
public class Filters {
    private BufferedImage bf1;
    private Raster data;
    private DataBuffer datab;
    private int width, height;
    private int datav [];
    private int datanew[];
    private BufferedImage bufnew;
    
    public Filters(BufferedImage bfi){
        bf1=bfi;
    }
    public BufferedImage getImg(){
            return bufnew;
    }
    
     public final static float R2 = (float)Math.sqrt(2);
    
    public final static float[] ROBERTS_V = {
		0,  0, -1,
		0,  1,  0,
		0,  0,  0,
	};
	public final static float[] ROBERTS_H = {
		-1,  0,  0,
		0,  1,  0,
		0,  0,  0,
	};
	public final static float[] PREWITT_V = {
		-1,  0,  1,
		-1,  0,  1,
		-1,  0,  1,
	};
	public final static float[] PREWITT_H = {
		-1, -1, -1,
		0,  0,  0,
		1,  1,  1,
	};
	public final static float[] SOBEL_V = {
		-1,  0,  1,
		-2,  0,  2,
		-1,  0,  1,
	};
	public static float[] SOBEL_H = {
		-1, -2, -1,
		0,  0,  0,
		1,  2,  1,
	};
	public final static float[] FREI_CHEN_V = {
		-1,  0,  1,
		-R2,  0,  R2,
		-1,  0,  1,
	};
	public static float[] FREI_CHEN_H = {
		-1, -R2, -1,
		0,  0,  0,
		1,  R2,  1,
	};
        
       
    
    //Blur Filter
    protected void Blur(){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        // Kernel Matrix 
        float[] matrix = {
            0.111f, 0.111f, 0.111f, 
            0.111f, 0.111f, 0.111f, 
            0.111f, 0.111f, 0.111f, 
        };
        // 1/9 = 0.11111111
        
        ConvolveFilter cf;
        cf = new ConvolveFilter(matrix);
        bufnew = cf.filter(bf1, bi);
        
        //Other deprecated way that didn't work :C 
        
        /* // Loop through every pixel in the image
        for (int y = 1; y < bf1.getHeight()-1; y++) {   // Skip top and bottom edges
          for (int x = 1; x < bf1.getWidth()-1; x++) {  // Skip left and right edges
            float sum = 0; // Kernel sum for this pixel
            for (int ky = -1; ky <= 1; ky++) {
              for (int kx = -1; kx <= 1; kx++) {
                // Calculate the adjacent pixel for this kernel point
                int pos = (y + ky)*bf1.getWidth() + (x + kx);
                // Image is grayscale, red/green/blue are identical
                int p1 = bf1.getRGB((x+kx),((y + ky)));
                float val = (p1>>16)&0xff;
                // Multiply adjacent pixels based on the kernel values
                sum += kernel[ky+1][kx+1] * val;
              }
            }
            // For this pixel in the new image, set the gray value
            // based on the sum from the kernel
            int colorP = Math.round(sum);
            bi.setRGB(x, y, colorP);
            
          }
        }
        */
    }
    //Heavy Average
    protected void HA(int n){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        // Kernel Matrix
        int param=n+8;
        System.out.println("Param="+param);
        float value=(float)1/param;
        System.out.println("Value="+value);
        float center=(float)n/param;
        System.out.println("Center="+center);
        float[] matrix = {
            value, value, value, 
            value, center, value, 
            value, value, value, 
        };
        for(int i=0;i<matrix.length;i++){
            float v=(float)matrix[i];
            System.out.printf("%.2f", v);
        }
        
        ConvolveFilter cf;
        cf = new ConvolveFilter(matrix);
        bufnew = cf.filter(bf1, bi);
    }
     //Gaussian
    protected void Gauss(){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        // Kernel Matrix 
        float a=(float)1/16;
        float b=(float)1/8;
        float c=(float)1/4;
        float[] matrix = {
            a, b, a, 
            b, c, b, 
            a, b, a, 
        }; 
        
        ConvolveFilter cf;
        cf = new ConvolveFilter(matrix);
        bufnew = cf.filter(bf1, bi);
        
    }
    //High Pass Basic
    protected void HPB(int n){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        // Kernel Matrix
        int param=9*n;
        float value=(float)1/param;
        float center=(float)(param-1)/param;
        float[] matrix = {
            value, value, value, 
            value, center, value, 
            value, value, value, 
        };
        for(int i=0;i<matrix.length;i++){
            System.out.print(matrix[i]+",");
        }
        
        ConvolveFilter cf;
        cf = new ConvolveFilter(matrix);
        bufnew = cf.filter(bf1, bi);
    }
    //Sobel Edge
    protected void Sobel(){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        // Kernel Matrix 
     
        
        ConvolveFilter cf;
        cf = new ConvolveFilter(SOBEL_H);
        BufferedImage temp = cf.filter(bf1, bi);
        cf = new ConvolveFilter(SOBEL_V);
        bufnew = cf.filter(temp, bi);
    }
    //Prewitt Edge
    protected void Prewitt(){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        
        
        ConvolveFilter cf;
        cf = new ConvolveFilter(PREWITT_H);
        BufferedImage temp = cf.filter(bf1, bi);
        cf = new ConvolveFilter(PREWITT_V);
        bufnew = cf.filter(temp, bi);
    }
    //Roberts Edge
    protected void Roberts(){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        // Kernel Matrix 
        ConvolveFilter cf;
        cf = new ConvolveFilter(ROBERTS_H);
        BufferedImage temp = cf.filter(bf1, bi);
        cf = new ConvolveFilter(ROBERTS_V);
        bufnew = cf.filter(temp, bi);
        
    }
    //Frei Chen Edge
    protected void FCE(){
        int width=bf1.getWidth();
        int height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        // Kernel Matrix 
     
        
        ConvolveFilter cf;
        cf = new ConvolveFilter(FREI_CHEN_H);
        BufferedImage temp = cf.filter(bf1, bi);
        cf = new ConvolveFilter(FREI_CHEN_V);
        bufnew = cf.filter(temp, bi);
    }
    
    
    
}
