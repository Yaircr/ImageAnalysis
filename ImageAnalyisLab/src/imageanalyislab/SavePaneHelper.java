/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author EduardoYair
 */
public class SavePaneHelper {
    private ArrayList<String> frames =new ArrayList<String>(); //list of Internal Frames Opened
    private JPanel jp;
    private int idx;
    SavePaneHelper(ArrayList<JImageFrame> frames){
        for(int i=0;i<frames.size();i++){
            this.frames.add(frames.get(i).getTitle().toString());
        }
    }
    protected int getIDX(){
        return idx;
    }
    protected void show(){
        JLabel sliderV= new JLabel("Elija Imagen a Salvar:");
        JComboBox <String> files = new JComboBox<>(new Vector<String>(frames));
        jp=new JPanel(new GridLayout(1,0));
        jp.add(sliderV);
        jp.add(files);
        
        int result = JOptionPane.showConfirmDialog(null, jp, "Salvar Imagen...",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            idx= files.getSelectedIndex();
        } else {
            JOptionPane.showMessageDialog(null, "Cancelado", "Salvar Imagen", 2);
        }
        
    }
}
