/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author EduardoYair
 */
public class SavePaneHelperC {
    private ArrayList<String> frames =new ArrayList<String>(); //list of Internal Frames Opened
    private String[] exts= {"png","gif","jpeg","jpg"};
    private JPanel jp;
    private int idx;
    private String name;
    private String extens;
    SavePaneHelperC(ArrayList<JImageFrame> frames){
        for(int i=0;i<frames.size();i++){
            this.frames.add(frames.get(i).getTitle().toString());
        }
    }
    protected int getIDX(){
        return idx;
    }
    protected String getFName(){
        return name;
    }
    protected String getFex(){
        return extens;
    }
    protected void show(){
        JLabel sliderV= new JLabel("Elija Imagen a Salvar:");
        JTextArea nameset=new JTextArea();
        nameset.setToolTipText("Nombre a Guardar");
        JComboBox <String> ext = new JComboBox<>(exts);
        JComboBox <String> files = new JComboBox<>(new Vector<String>(frames));
        jp=new JPanel(new GridLayout(0,1));
        jp.add(sliderV);
        jp.add(files);
        jp.add(nameset);
        //jp.add(ext);
        int result = JOptionPane.showConfirmDialog(null, jp, "Salvar Imagen Como...",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            idx= files.getSelectedIndex();
            name=nameset.getText();
            //extens=files.getSelectedItem().toString();
        } else {
            JOptionPane.showMessageDialog(null, "Cancelado", "Salvar Imagen Como", 2);
        }
        
    }
}
