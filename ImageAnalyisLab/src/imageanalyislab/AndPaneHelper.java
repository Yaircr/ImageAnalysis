/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author EduardoYair
 */
public class AndPaneHelper {
    private ArrayList<String> frames =new ArrayList<String>(); //list of Internal Frames Opened
    private JPanel jp;
    private int idx1,idx2;
    AndPaneHelper(ArrayList<JImageFrame> frames){
        for(int i=0;i<frames.size();i++){
            this.frames.add(frames.get(i).getTitle().toString());
        }
    }
    protected int getIDX1(){
        return idx1;
    }
    protected int getIDX2(){
        return idx2;
    }
    protected void show(){
        
        JComboBox <String> files = new JComboBox<>(new Vector<String>(frames));
        JComboBox <String> files2 = new JComboBox<>(new Vector<String>(frames));
        jp=new JPanel(new GridLayout(0,1));
        jp.add(files);
        jp.add(files2);
        int result = JOptionPane.showConfirmDialog(null, jp, "AND de Imagenes",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            idx1= files.getSelectedIndex();
            idx2= files2.getSelectedIndex();
        } else {
            JOptionPane.showMessageDialog(null, "Cancelado", "AND de Imagenes", 2);
        }
        
    }
    
}
