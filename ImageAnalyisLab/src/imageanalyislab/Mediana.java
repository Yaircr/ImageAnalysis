package imageanalyislab;

import java.awt.Color;
import java.awt.image.BufferedImage;


public class Mediana {
    private BufferedImage bf;
    private int width, height;
    Integer num[]=new Integer[9];
    public Mediana(BufferedImage imagen){
        bf=imagen; 
    }
    public BufferedImage addnegro(){
        //Nuevo tamaño de la imagen
        width=bf.getWidth();
        height=bf.getHeight();
        int nw=width+2;
        int nh=height+2;
        int rojo=0,verde=0,azul=0;
        BufferedImage bi = new BufferedImage(nw,nh,BufferedImage.TYPE_INT_RGB);
        //Rellenamos la imagen
        for (int i = 0; i <width; i++) {
            for (int j = 0; j < height; j++) {
                if(i==0 || j==0 || i==width-1 || j==height-1){
                    rojo=0;
                    verde=0;
                    azul=0;                    
                }else{
                int centro =bf.getRGB(i-1,j-1); 
                Color c= new Color(centro);
                rojo =c.getRed();
                verde =c.getGreen();
                azul =c.getBlue();  
                }
                Color cnew= new Color(rojo,verde,azul);
               bi.setRGB(i, j,cnew.getRGB()); 
            }
            
        }
        return bi; 
    }
    public BufferedImage mediana(){
     int r=0,g=0,b=0;
        BufferedImage bi= addnegro();
        BufferedImage p = new BufferedImage(bi.getWidth(),bi.getHeight(),BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i <=bi.getWidth() ; i++) {
            for (int j = 0; j <= bi.getHeight(); j++) {
                //Filtro de 3x3
                if(i!=0 && j!=0 && i<bi.getWidth()-1 && j<bi.getHeight()-1){
                            int centro =bi.getRGB(i,j); 
                            Color cc= new Color(centro);
                            int cd =bi.getRGB(i,j+1); 
                            Color ccd= new Color(cd);
                            int cb =bi.getRGB(i+1,j); 
                            Color ccb= new Color(cb);
                            int ead =bi.getRGB(i+1,j+1); 
                            Color cead= new Color(ead);
                            int ei =bi.getRGB(i-1,j-1); 
                            Color cei= new Color(ei);
                            int ca =bi.getRGB(i-1,j); 
                            Color cca= new Color(ca);
                            int ed =bi.getRGB(i-1,j+1);  
                            Color ced= new Color(ed);
                            int ci =bi.getRGB(i,j-1); 
                            Color cci= new Color(ci);
                            int eai =bi.getRGB(i+1,j-1); 
                            Color ceai= new Color(eai);
                            num[0]=cc.getRed();
                            num[1]=cca.getRed();
                            num[2]=ccb.getRed();
                            num[3]=cci.getRed();
                            num[4]=ccd.getRed();
                            num[5]=cei.getRed();
                            num[6]=ced.getRed();
                            num[7]=ceai.getRed();
                            num[8]=cead.getRed();
                            r=menorMayor();
                            num[0]=cc.getGreen();
                            num[1]=cca.getGreen();
                            num[2]=ccb.getGreen();
                            num[3]=cci.getGreen();
                            num[4]=ccd.getGreen();
                            num[5]=cei.getGreen();
                            num[6]=ced.getGreen();
                            num[7]=ceai.getGreen();
                            num[8]=cead.getGreen();
                            g=menorMayor();
                            num[0]=cc.getBlue();
                            num[1]=cca.getBlue();
                            num[2]=ccb.getBlue();
                            num[3]=cci.getBlue();
                            num[4]=ccd.getBlue();
                            num[5]=cei.getBlue();
                            num[6]=ced.getBlue();
                            num[7]=ceai.getBlue();
                            num[8]=cead.getBlue();
                            b=menorMayor();
                            Color cnew= new Color(r,g,b);
                            p.setRGB(i, j,cnew.getRGB());
                }  
            }  
        }
        return p;   
    }
    
    public int menorMayor(){
        int aux;
		for(int i = 0; i < num.length; i++){
			for(int j=i+1; j < num.length; j++){
				if(num[i] > num[j]){
					aux = num[i];
					num[i] = num[j];
					num[j] = aux;
				}
			}
		}
           return num[4];
	}
    
    
}
