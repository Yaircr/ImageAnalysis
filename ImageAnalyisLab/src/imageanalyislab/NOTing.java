/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;

/**
 *
 * @author EduardoYair
 */
public class NOTing {
    private BufferedImage bf1,bf2;
    private Raster data;
    private DataBuffer datab;
    private int width, height;
    private int datav [];
    private int datanew[];
    private BufferedImage bufnew;
    
    public NOTing(BufferedImage bfi){
        bf1=bfi;
    }
     public void doNot(){
  
         
        width=bf1.getWidth();
        height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
              int p1 = bf1.getRGB(x,y);

              
              int r = (p1>>16)&0xff;
              int g = (p1>>8)&0xff;
              int b = (p1>>0)&0xff;

              if(r==255){
                    r= r & 0x00;
                }else{
                    r= r | 0xFF;
                }
                if(g==255){
                    g= g & 0x00;
                }else{
                     g= g | 0xFF;
                }
                if(b==255){
                    b= b & 0x00;
                }else{
                     b= b | 0xFF;
                }
              
              //replace RGB value with avg
              p1 = (p1& 0xff000000) | (r<<16) | (g<<8) | b<<0;

              bi.setRGB(x, y, p1);
            }
        }
      
        bufnew=bi;
             
        }
     public BufferedImage getImg(){
             
            return bufnew;
        }
}
