/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

/**
 *
 * @author EduardoYair
 */
public class JImageFrame extends JInternalFrame{
        private File img;
        ImagePane imagePane;
        BufferedImage bf;
        
    public JImageFrame(File img) throws IOException{
        
        this.img = img;
        setUpFrame();
    }
    public JImageFrame(BufferedImage bfi) throws IOException{
        
        this.bf = bfi;
        setUpFrame();
    }
    protected ImagePane getIP(){
        return imagePane;
    }
    public void setUpFrame() throws IOException {
        JPanel display = new JPanel();
        imagePane= new ImagePane();
        if(bf!=null){
            imagePane.setIMG(bf);
        }else{
            imagePane.setImageSource(img);
        }
        
        
        display.add(imagePane);
        display.setVisible(true);
        
        this.add(display);
        this.setSize(imagePane.getDimX(),imagePane.getDimY());
        this.setLocation(100, 100);
        this.setIconifiable(true);
        this.setClosable(true);
        this.pack();
        this.setVisible(true);
    }
}    