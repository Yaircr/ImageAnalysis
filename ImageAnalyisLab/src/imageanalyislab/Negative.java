/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;

/**
 *
 * @author alumno
 */
public class Negative {
     private int ThreshA=255;
    private int ThreshB=0;
    private BufferedImage bf;
    private Raster data;
    private DataBuffer datab;
    private   int width, height;
    private int datav [];
    private int datanew[];
    private BufferedImage bufnew;
    
    public Negative(BufferedImage bfi){
        bf=bfi;
        
    }
     public void DoNegative(){
  
         
        width=bf.getWidth();
        height=bf.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf.getType());
        for(int i=0;i<width;i++) {
            for(int j=0;j<height;j++) {
                int rgb =bf.getRGB(i,j); 
                int r =255- (rgb >> 16) & 0xff;
                int g =255- (rgb >> 8) & 0xff;
                int b =255- (rgb >> 0) & 0xff;
                 
                // Return the result
                rgb= (rgb & 0xff000000) | (r << 16) | (g << 8) | (b << 0);
                bi.setRGB(i, j,rgb);
            }
      
        bufnew=bi;
             
        }
    }
     public BufferedImage getImg(){
             
            return bufnew;
        }
}
