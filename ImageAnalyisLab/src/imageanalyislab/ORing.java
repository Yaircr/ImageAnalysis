/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;

/**
 *
 * @author EduardoYair
 */
public class ORing {
    private BufferedImage bf1,bf2;
    private Raster data;
    private DataBuffer datab;
    private int width, height;
    private int datav [];
    private int datanew[];
    private BufferedImage bufnew;
    
    public ORing(BufferedImage bfi,BufferedImage bfii){
        bf1=bfi;
        bf2=bfii;
    }
     public void doOr(){
  
         
        width=bf1.getWidth();
        height=bf1.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf1.getType());
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
              int p1 = bf1.getRGB(x,y);

              int a1 = p1 & 0xff000000;
              int r1 = (p1>>16)&0xff;
              int g1 = (p1>>8)&0xff;
              int b1 = (p1>>0)&0xff;

              int p2 = bf2.getRGB(x,y);

              int a2 = p2 & 0xff000000;
              int r2 = (p2>>16)&0xff;
              int g2 = (p2>>8)&0xff;
              int b2 = (p2>>0)&0xff;
              
              int a=a1|a2;
              int r=r1|r2;
              int g=g1|g2;
              int b=b1|b2;
              
              //replace RGB value with avg
              p1 = (a<<24) | (r<<16) | (g<<8) | b<<0;

              bi.setRGB(x, y, p1);
            }
        }
      
        bufnew=bi;
             
        }
     public BufferedImage getImg(){
             
            return bufnew;
        }
}
