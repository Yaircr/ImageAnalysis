/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;

/**
 *
 * @author EduardoYair
 */
public class SplitChannels { private BufferedImage bf;
    private Raster data;
    private DataBuffer datab;
    private int width, height;
    private int datav [];
    private int datanew[];
    private BufferedImage bufRed, bufGre, bufBlu;
    
    public SplitChannels(BufferedImage bfi){
        bf=bfi;
    }
     public void doRGB(){
  
         
        width=bf.getWidth();
        height=bf.getHeight();
        bufRed = new BufferedImage(width,  height,bf.getType());
        bufGre = new BufferedImage(width,  height,bf.getType());
        bufBlu = new BufferedImage(width,  height,bf.getType());
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                int p = bf.getRGB(x,y);
                int a = p & 0xff000000;
                int red = (p >> 16) & 0xff;
                int green = (p >> 8) & 0xff;
                int blue = (p) & 0xff;
                bufRed.setRGB(x, y, a | (red << 16));
                bufGre.setRGB(x, y, a | (green << 8));
                bufBlu.setRGB(x, y, a | blue);
            }
        }
             
        }
     public BufferedImage getRed(){
             
            return bufRed;
        }
     public BufferedImage getGreen(){
             
            return bufGre;
        }
     public BufferedImage getBlue(){
             
            return bufBlu;
        }
}