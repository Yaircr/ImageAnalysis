/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author EduardoYair
 */
public class BinaryPaneHelper {
    private ArrayList<String> frames =new ArrayList<String>(); //list of Internal Frames Opened
    private JPanel jp;
    JCheckBox otsu;
    JCheckBox inv;
    private int idx,thr;
    BinaryPaneHelper(ArrayList<JImageFrame> frames){
        for(int i=0;i<frames.size();i++){
            this.frames.add(frames.get(i).getTitle().toString());
        }
    }
    protected int getIDX(){
        return idx;
    }
    protected int getTHR(){
        return thr;
    }
    protected void show(){
        JLabel sliderV= new JLabel("Umbral: "+0);
        otsu=new JCheckBox("Ōtsu");
        inv=new JCheckBox("Inverso");
        sliderV.setHorizontalAlignment(JLabel.CENTER);
        JSlider sliderT= new JSlider(0,255);
        sliderT.addChangeListener(new ChangeListener(){
             @Override
            public void stateChanged(ChangeEvent e) {
                sliderV.setText("Umbral: "+String.valueOf(sliderT.getValue()));
            }
        });
        
        JComboBox <String> files = new JComboBox<>(new Vector<String>(frames));
        jp=new JPanel(new GridLayout(0,1));
        jp.add(files);
        jp.add(sliderT);
        jp.add(sliderV);
        jp.add(otsu);
        jp.add(inv);
        int result = JOptionPane.showConfirmDialog(null, jp, "Binarizacion",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            idx= files.getSelectedIndex();
            thr=sliderT.getValue();
        } else {
            JOptionPane.showMessageDialog(null, "Cancelado", "Binarizacion", 2);
        }
        
    }
    
}
