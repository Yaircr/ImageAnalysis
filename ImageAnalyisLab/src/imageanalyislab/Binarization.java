/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageanalyislab;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;

/**
 *
 * @author EduardoYair
 */
public class Binarization {
    private int ThreshA=255;
    private int ThreshB=0;
    private BufferedImage bf;
    private Raster data;
    private DataBuffer datab;
    private int width, height;
    private int threshold;
    private int datav [];
    private int datanew[];
    private BufferedImage bufnew;
    
    public Binarization(BufferedImage bfi, int threshold){
        bf=bfi;
        this.threshold=threshold;
    }
     public void DoBinirization(){
  
         
        width=bf.getWidth();
        height=bf.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf.getType());
        for(int i=0;i<width;i++) {
            for(int j=0;j<height;j++) {
                int rgb =bf.getRGB(i,j); 
                int r = (rgb >> 16) & 0xff;
                int g = (rgb >> 8) & 0xff;
                int b = (rgb >> 0) & 0xff;
                 
            // Calculate the brightness
            if(r>threshold||g>threshold||b>threshold)
            {
                r=255;
                g=255;
                b=255;
            }
            else
            {
               r=0;
               g=0;
               b=0;
            } 
                // Return the result
                rgb= (rgb & 0xff000000) | (r << 16) | (g << 8) | (b << 0);
                bi.setRGB(i, j,rgb);
            }
      
        bufnew=bi;
             
        }
    }
     public void DoBinirizationINV(){
  
         
        width=bf.getWidth();
        height=bf.getHeight();
        BufferedImage bi = new BufferedImage(width,  height,bf.getType());
        for(int i=0;i<width;i++) {
            for(int j=0;j<height;j++) {
                int rgb =bf.getRGB(i,j); 
                int r = (rgb >> 16) & 0xff;
                int g = (rgb >> 8) & 0xff;
                int b = (rgb >> 0) & 0xff;
                 
            // Calculate the brightness
            if(r<threshold||g<threshold||b<threshold)
            {
                r=255;
                g=255;
                b=255;
            }
            else
            {
               r=0;
               g=0;
               b=0;
            } 
                // Return the result
                rgb= (rgb & 0xff000000) | (r << 16) | (g << 8) | (b << 0);
                bi.setRGB(i, j,rgb);
            }
      
        bufnew=bi;
             
        }
    }
         
        public BufferedImage getImg(){
             
            return bufnew;
        }
         
         
         
    }